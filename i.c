/*
gcc i.c -lm -Wall
./a.out

 after 4000000 iterations z moved 0.003345 and still has 0.011387 to fixed point
 after 4000000 iterations z moved 0.003345 and still has 0.011387 to fixed point 
 after 40000 iterations z moved   0.000050 and still has 0.013449 to fixed point 

 after        4 iterations z moved 0.0000000050117063 and still has 0.0134587581012325 to fixed point 
 after       40 iterations z moved 0.0000000501169821 and still has 0.0134587493030271 to fixed point 
 after      400 iterations z moved 0.0000005011617400 and still has 0.0134586612907479 to fixed point 
 after     4000 iterations z moved 0.0000050108069362 and still has 0.0134577781475032 to fixed point 
 after    40000 iterations z moved 0.0000500246399044 and still has 0.0134486467159642 to fixed point 
 after   400000 iterations z moved 0.0004897196186683 and still has 0.0133296117811871 to fixed point 
 after  4000000 iterations z moved 0.0033449601070921 and still has 0.0113869977319601 to fixed point 
after  40000000 iterations z moved 0.0073507376766378 and still has 0.0068052112314813 to fixed point 
after 400000000 iterations z moved 0.0099300022520497 and still has 0.0038334379135754 to fixed point



gnuplot
set terminal png
set output "d.png"
set xtics rotate by -90
set xlabel "zx"
set ylabel "zy"
set title "orbit of z under fc(z)=z^2+c"
set label "z"
plot "data.txt" using 1:2
set label "z fixed"
plot "fixed.txt" 


plot "data.txt" using 1:2,"data.txt" using 4:5;

plot "data.txt" i 0 l "z", "fixed.txt i 1 l "fixed"
plot "data.txt" using 1:2,"data.txt" using 4:5;






(%i1) f(z):=z*z+c;
(%o1)                           f(z) := z z + c
(%i2) z1:f(z);
                                     2
(%o2)                               z  + c
(%i3) z4:f(f(f(f(z))));
                              2     2     2     2
(%o3)                     (((z  + c)  + c)  + c)  + c
(%i4) expand(z4);
                           16        14       2  12        12       3  10       2  10       4  8
(%o4) z   + 8 c z   + 28 c  z   + 4 c z   + 56 c  z   + 24 c  z   + 70 c  z



       3  8      2  8        8       5  6       4  6       3  6      2  6
 + 60 c  z  + 6 c  z  + 2 c z  + 56 c  z  + 80 c  z  + 24 c  z  + 8 c  z


       6  4       5  4       4  4       3  4      2  4      7  2       6  2
 + 28 c  z  + 60 c  z  + 36 c  z  + 16 c  z  + 4 c  z  + 8 c  z  + 24 c  z

       5  2       4  2      3  2    8      7      6      5      4      3    2
 + 24 c  z  + 16 c  z  + 8 c  z  + c  + 4 c  + 6 c  + 6 c  + 5 c  + 2 c  + c

 + c










*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>












int main(void) {
  int i, j;
  int jMax = 100000000;
  double x,y;
  double x0,y0;
  double xa, ya; // parabolic alfa fixed point Za  = 0.0000000000000001 ; 0.5000000000000000 
  //double xt,yt; // temp
  double x2, y2;
  double cx, cy;
  double d, dx, dy; // d =  = distance between points = abs(z(p+n) - z(n))
  double da, dax, day; // dp =  = distance betweeen points = abs(za - zn)
  int period = 4; 

  char *output_filename="data.txt";
  FILE *output_file;
  output_file = fopen(output_filename, "w");  
  if (output_file  == NULL) {
    fprintf(stderr, "Nie moge otworzyc %s\n", output_filename);
    return 1; }
    

    //  parabolic alfa fixed point Za  = 0.0000000000000001 ; 0.5000000000000000 
    xa = 0.0;
    ya = 0.5;
    //  c  = 0.2500000000000001 , 0.5
    cx =  0.25;
    cy =  0.5 ;



    /* nagłówek */
    fprintf(output_file,"%s %s      %s      %s  %s %s \n","#","x","y", "d" ,"x", "y");
    
    // unknown Z  = -0.0061745136621478 ; 0.4880411715884660 
    y0 = 0.4880411715884660;
    x0 = -0.0061745136621478;
    //
    x = x0;
    y = y0;
    //
    dx=xa-x;
    dy = ya-y;
    d = sqrt(dx*dx+dy*dy);
    
    /* 2 kolumny liczb rozdzielone spacjami */
    fprintf(output_file,"%1.16f %1.16f %1.16f %1.16f %1.16f \n",x,y, d, xa, ya);

    //
    x2=x*x;
    y2=y*y;


    for(j=0;j<jMax ;++j){
     for(i=0;i<period ;++i) {
          // z= z^2 + c
          y=2*x*y + cy;
          x=x2-y2 + cx;
          x2=x*x;
          y2=y*y;
          
          

    }; /* for(i */
  
   //
          dx=xa-x;
          dy = ya-y;
          d = sqrt(dx*dx+dy*dy);

          /* 2 kolumny liczb rozdzielone spacjami */
          fprintf(output_file,"%1.16f %1.16f %1.16f \n",x,y, d);

   }



  fclose(output_file);

  fprintf(stderr,"file %s saved\n ", output_filename);
  //
  dx = x0-x;
  dy = y0-y;
  d  = sqrt(dx*dx+dy*dy);
  //
  dax = xa-x;
  day = ya-y;
  da = sqrt(dax*dax+day*day); 
  
  printf("after %d iterations z moved %.16f and still has %.16f to fixed point \n",jMax*period, d, da);
  
  return 0;
}
